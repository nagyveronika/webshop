package hu.citec.webshop;

import javax.sql.DataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

@Configuration
@ComponentScan("hu.citec.webshop")
public class MainConfig {

	@Bean
	public JdbcTemplate jdbcTemplate() throws ClassNotFoundException {
		return new JdbcTemplate(dataSource());
	}

	private DataSource dataSource() throws ClassNotFoundException {
		Class.forName("com.mysql.jdbc.Driver");
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setUrl("jdbc:mysql://localhost:3306/webshop");
		dataSource.setUsername("root");
		dataSource.setPassword("");
		return dataSource;
	}
}
