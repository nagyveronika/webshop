package hu.citec.webshop.repository;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import hu.citec.webshop.entities.Product;
import hu.citec.webshop.entities.User;

@Repository
public class WebshopRepository {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;	

	public User validateUser(String username, String pass) {
		String query = "SELECT username as username, user_id as id, full_name as fullname, user_password as pass FROM users WHERE username =? AND user_password=?";
		return jdbcTemplate.queryForObject(query, BeanPropertyRowMapper.newInstance(User.class), username, pass);
		
	}
	
	public List<Product> findProducts() {
		String query = "SELECT product_id as productId, name, type, price FROM product";
		return jdbcTemplate.query(query, BeanPropertyRowMapper.newInstance(Product.class));
				
	}
	
	public Product findProductyById(int productId) {
		String query = "SELECT product_id as productId, name, type, price FROM product WHERE product_id = ?";
		return jdbcTemplate.queryForObject(query, BeanPropertyRowMapper.newInstance(Product.class), productId);
	}

	public void addProduct(Product product) {
		String query = "INSERT INTO product (name, type, price) VALUES (?, ?, ?)";
		jdbcTemplate.update(query, product.getName(), product.getType(), product.getPrice());
	}

	public void editProduct(Product product) {
		String query = "UPDATE product SET name = ?, type = ?, price = ? WHERE product_id = ?";
		jdbcTemplate.update(query, product.getName(), product.getType(), product.getPrice(), product.getProductId());
	}
	
	public void removeProductById(int productId) {
		String query = "DELETE FROM product WHERE product_id = ?";
		jdbcTemplate.update(query, ps -> ps.setInt(1, productId));
	}
	
}
