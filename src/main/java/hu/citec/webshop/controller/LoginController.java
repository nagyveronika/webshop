package hu.citec.webshop.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import hu.citec.webshop.entities.User;
import hu.citec.webshop.service.WebshopService;

@Controller
public class LoginController {
	
	@Autowired
	private WebshopService service;
	
	@Autowired
	private HttpServletRequest request;
	
	@PostMapping("/")
	public String home() {
		return "index";
	}

	@GetMapping("/login")
	public String login(Model model) {
		model.addAttribute("user", new User());
		return "login";
	}
	
	@PostMapping("/login")
	public String doLogin(@RequestParam String username, @RequestParam String pass, Model model) {
	
		 User findUserByName = service.findUserByName(username, pass);
		 
			if (findUserByName != null) {
				HttpSession session = request.getSession(); 
			} else {
				model.addAttribute("wrong", "error");
				return "index";
			}
		 

		return "redirect:/products/";
	}
	
}
