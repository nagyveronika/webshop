package hu.citec.webshop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import hu.citec.webshop.entities.Product;
import hu.citec.webshop.service.WebshopService;

@Controller
public class ProductController {
	
	@Autowired
	private WebshopService service;

	@GetMapping("/products")
	public String getProducts(Model model) {
		model.addAttribute("products", service.findProducts());
		return "products";
	}
	
	@GetMapping("/add_products")
	public String createNewProduct(Model model) {
		model.addAttribute("product", new Product());

		return "add_products";
	}
	
	@PostMapping("/add_products")
	public String addNewProduct(@ModelAttribute Product product) {
		service.addProduct(product);
		return "redirect:/products";
	}
	
	@GetMapping("/edit_products/{productId}")
	public String editProduct(@PathVariable int productId, Model model) {
		model.addAttribute("product", service.findProductyById(productId));
		return "edit_products";
	}
	
	@PostMapping("/edit_products")
	public String editProductById(@ModelAttribute Product product) {
		service.editProduct(product);

		return "redirect:/products/";
	}
	
	@GetMapping("/delete/{productId}")
	public String deleteProduct(@PathVariable int productId) {	
		service.removeProductById(productId);
		return "redirect:/products/";
	}
	
}
