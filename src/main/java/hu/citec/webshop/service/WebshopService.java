package hu.citec.webshop.service;

import java.util.List;

import org.springframework.stereotype.Service;

import hu.citec.webshop.entities.Product;
import hu.citec.webshop.entities.User;
import hu.citec.webshop.repository.WebshopRepository;

@Service
public class WebshopService {
	private WebshopRepository repository;
	
	public WebshopService (WebshopRepository repository) {
		this.repository = repository;
	}
	
	public User findUserByName(String username, String pass) {
		return repository.validateUser(username, pass);
	}
	
	public List<Product> findProducts() {
		return repository.findProducts();
		
	}
	
	public Product findProductyById(int productId) {
		return repository.findProductyById(productId);
	}

	public void addProduct(Product product) {
		repository.addProduct(product);
	}

	public void editProduct(Product product) {
		repository.editProduct(product);
	}
	
	public void removeProductById(int productId) {
		repository.removeProductById(productId);
		
	}
}
