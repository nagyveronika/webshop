package hu.citec.webshop.entities;

public class Product {
	private Integer productId;
	private String name;
	private String type;
	private Double price;
	
	public Product() {
		
	}
	
	
	public Product(Integer productId, String name, String type, Double price) {
		this.productId = productId;
		this.name = name;
		this.type = type;
		this.price = price;
	}

	

	public Integer getProductId() {
		return productId;
	}


	public void setProductId(Integer productId) {
		this.productId = productId;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public Double getPrice() {
		return price;
	}


	public void setPrice(Double price) {
		this.price = price;
	}


	@Override
	public String toString() {
		return "Product [productId=" + productId + ", name=" + name + ", type=" + type + ", price=" + price + "]";
	}
	
	
}
