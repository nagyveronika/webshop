package hu.citec.webshop.entities;

public class User {
	private int id;
	private String fullname;
	private String username;
	private String pass;

	public User() {

	}

	public User(int id, String fullname, String username, String pass) {
		this.id = id;
		this.fullname = fullname;
		this.username = username;
		this.pass = pass;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", fullname=" + fullname + ", username=" + username + ", pass=" + pass + "]";
	}
	
	

}
