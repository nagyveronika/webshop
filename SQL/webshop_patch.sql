
CREATE TABLE users(
  user_id INT NOT NULL AUTO_INCREMENT,
  full_name VARCHAR(100) NOT NULL,
  username VARCHAR(50) NOT NULL UNIQUE,
  user_password  VARCHAR(100) NOT NULL,
  
  PRIMARY KEY(user_id)
);

CREATE TABLE product(
  product_id INT NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  type VARCHAR(30) NOT NULL,
  price DOUBLE NOT NULL, 

  PRIMARY KEY(product_id),
  CONSTRAINT uc_name_type UNIQUE (name , type)
);


INSERT INTO `users`(`full_name`, `username`, `user_password`)
VALUES 
('Teszt Elek', 'tesztelek','teszt1234'),
('Adminisztrátor','admin','admin');

INSERT INTO `product`(`name`, `type`, `price`) VALUES 
('TV','SAMSUNG',120000);